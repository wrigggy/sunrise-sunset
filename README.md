# Sunrise Sunset

## Run tests

```
npm install
npm t
```

## Run prod

```
npm install
npm run start
```

## Notes
Haven't used Rambda before but I'm keen on functional programming these (due to react), so I will be trying it out soon.

I've kept it as functional as I can, but i'm sure there's room for improvement.

I think the sunrise api might have blocked my IP address or something (I may have sent 100 concurrent requests by accident a few times...) so I haven't tested the prod build yet, but if my unit testing is good enough then it might work anyway.

I will check later today/tomorrow to see if i can make requests, and I may apply a fix then, if it's broken.