type SunriseAndDaylength = {
  sunrise: string,
  daylength: string
};

export { SunriseAndDaylength };