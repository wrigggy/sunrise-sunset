import async from 'async';
import { findEarliest } from './earliest/earliest';

import { generateLatLongs } from "./lat-long/lat-long";
import { getSunriseAndDayLength } from "./sunrise/sunrise";
import { SunriseAndDaylength } from "./types";

const run = async () => {

  try {

    const latLongs: Array<string> = generateLatLongs(100);
  
    // I'm using async.map series here to ensure that the
    // requests happen in series. 
    // I'm guessing rambda has a nice way to handle this 
    // (including batching them 5 at time) but I haven't 
    // tried rambda yet so idk
    const sunrises: Array<SunriseAndDaylength> = 
      await async.mapSeries(latLongs, async (latLong: string) => {
        console.log("Fetching one at a time...");
        return await getSunriseAndDayLength(latLong);
      });
    
    const earliest = findEarliest(sunrises);

    console.log(`Earliest sunrise is at ${earliest.sunrise}
      with day length of ${earliest.daylength}`);

  } catch(e) {
    console.error(e)
  }
};

run();