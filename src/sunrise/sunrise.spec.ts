import axios from "axios";
import MockAdapter from "axios-mock-adapter";
import { SunriseAndDaylength } from "../types";
import { getSunriseAndDayLength } from './sunrise';

describe('sunrise', () => {
  const latLong = "-67,45";
  const mock = new MockAdapter(axios);

  afterEach(() => {
    mock.reset();
  });

  it('should throw error if cannot reach api', async () => {
    let errorThrown = false;

    try {
      await getSunriseAndDayLength(latLong, "wrongEndpoint.localhost");
    } catch(e) {
      errorThrown = true;
    }

    expect(errorThrown).toBe(true);
  });

  it('should throw error if correct data isn\'t retrieved', async () => {
    let errorThrown = false;

    mock.onGet("/endpoint").reply(200, {
      "results": {
        "wrongData": "true",
        "somethingElse": "hello world"
      }
    });

    try {
      await getSunriseAndDayLength(latLong, "endpoint");
    } catch(e) {
      errorThrown = true;
    }

    expect(errorThrown).toBe(true);
  });

  it('should throw an error if api returns non 200 status', async () => {
    let errorThrown = false;

    mock.onGet("/endpoint").reply(404, {
      "results": {
        "sunrise": "foo",
        "day_length": "bar"
      }
    });

    try {
      await getSunriseAndDayLength(latLong, "endpoint");
    } catch(e) {
      errorThrown = true;
    }

    expect(errorThrown).toBe(true);
  });

  it('should be able to retrieve sunrise and daylength data', async () => {

    mock.onGet("https://api.sunrise-sunset.org/json?lat=-67&lng=45")
      .reply(200, {
        "results": {
          "sunrise": "correct sunrise",
          "day_length": "correct day length"
        }
    });

    let sunriseAndDaylength: SunriseAndDaylength;

    try {
      sunriseAndDaylength = await getSunriseAndDayLength(latLong);
    } catch(e) {
    }

    expect(sunriseAndDaylength.sunrise).toBe("correct sunrise");
    expect(sunriseAndDaylength.daylength).toBe("correct day length");
  });
});