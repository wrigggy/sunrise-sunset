import axios from "axios";
import { SunriseAndDaylength } from "../types";

type Response = {
  data: {
    results: {
      sunrise: string,
      day_length: string
    }
  }
};

const getSunriseAndDayLength = 
  async (latLong: string, endpoint: string = "https://api.sunrise-sunset.org/json")
  : Promise<SunriseAndDaylength> => {

  const splitLatLong = latLong.split(',');

  const fullEndpoint = `${endpoint}?lat=${splitLatLong[0]}&lng=${splitLatLong[1]}`;

  const { data } = await axios.get<{}, Response>(fullEndpoint);

  if(!data.results) {
    throw new Error("Sunrise sunset api returned incorrect data");
  }

  if(!data.results 
    || !data.results.sunrise 
    || !data.results.day_length) {

    throw new Error("Sunrise sunset api returned incorrect data");
  }

  return { 
    sunrise: data.results.sunrise,
    daylength: data.results.day_length
  }
};

export { getSunriseAndDayLength };