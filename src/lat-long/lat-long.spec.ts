import { generateLatLong, generateLatLongs } from './lat-long';

describe('lat-long', () => {

  const isValidLatLong = (latLong: string) => {
    const regex = /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?),\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/;
    return latLong.match(regex) !== null;
  }

  it('should be able to generate a lat / long point', () => {
    const latLong = generateLatLong();
    expect(isValidLatLong(latLong)).toBe(true);
  });

  it('should be able to generate multiple lat / long points', () => {
    const latLongs = generateLatLongs(100);
    expect(latLongs.length).toBe(100);

    latLongs.forEach((latLong: string) => {
      expect(isValidLatLong(latLong)).toBe(true);
    });
  })

  it('lat / long point should be random', () => {
    // TODO I'm not really sure how to test randomness?
    // Is it even possible?
  });
});