
const generateLatLong = (): string => {
  const lat = (Math.random() - 0.5) * 90;
  const long = (Math.random() - 0.5) * 180;

  return `${lat},${long}`;
};

const generateLatLongs = (count: number): Array<string> => {
  return Array.apply(null, Array(count))
    .map(() => generateLatLong());
};

export { generateLatLong, generateLatLongs };