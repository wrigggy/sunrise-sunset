import moment from "moment";
import { SunriseAndDaylength } from "../types";

const findEarliest = (sunrise: Array<SunriseAndDaylength>) : SunriseAndDaylength => {

  if(!sunrise.length) {
    return { sunrise: "", daylength: "" };
  }

  return sunrise
    .sort((a: SunriseAndDaylength, b: SunriseAndDaylength) => {

    const dateA = moment(a.sunrise, 'HH:mm:ss');
    const dateB = moment(b.sunrise, 'HH:mm:ss');

    if(dateA > dateB) {
      return 1;
    }

    return -1;
  })[0];
};

export { findEarliest };