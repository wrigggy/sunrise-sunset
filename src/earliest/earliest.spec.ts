import { SunriseAndDaylength } from '../types';
import { findEarliest } from './earliest';

describe('earliest', () => {

  const sunrises: Array<SunriseAndDaylength> = [{
    sunrise: "7:05:41 AM",
    daylength: "11:16:57"
  }, {
    sunrise: "3:05:41 AM",
    daylength: "12:16:57"
  }, {
    sunrise: "3:02:41 AM",
    daylength: "13:16:57"
  }, {
    sunrise: "10:05:41 AM",
    daylength: "14:16:57"
  }, {
    sunrise: "5:05:41 AM",
    daylength: "15:16:57"
  }];

  it('should be able to find the earliest sunrise', () => {
    const earliest = findEarliest(sunrises);

    expect(earliest).toEqual({
      sunrise: "3:02:41 AM",
      daylength: "13:16:57"
    });
  });
});